// handle faq home section2 questionType
window.onload = function () {
  var questionType = [
    {
      id: 1,
      name: "Basic functions",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Block a number manually",
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 2,
      name: "Settings and customization",
      questionItems: [
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 3,
      name: "Messaging",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 4,
      name: "Calls",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 5,
      name: "Contacts",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 6,
      name: "Tips and tricks",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 7,
      name: "Warranty",
      questionItems: [
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 8,
      name: "Service Center",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 9,
      name: "Messaging",
      questionItems: ["Block a number manually", "Browse in left-hand mode"],
    },
    {
      id: 10,
      name: "Multimedia, apps, photo, video, music",
      questionItems: [
        "Block a number manually",
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 11,
      name: "Messaging",
      questionItems: [
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
    {
      id: 12,
      name: "Multimedia, apps, photo, video",
      questionItems: [
        "Browse in left-hand mode",
        "Change typing language in messaging app (SMS/MMS)",
        "Check IMEI code",
        "Check the model of the smartphone",
        "Connect an USB device (mouse, keyboard, USB key, etc.) using OTG function",
        "Divide the screen to display 2 apps (splitscreen mode)",
        "Keep the original file of a retouched photo",
        "Limit data usage",
        "Locate your smartphone and more using Android™ Device Manager",
      ],
    },
  ];
  var pageIndex = 1;
  var questionInfoItems = questionType[0].questionItems;
  handleQuestionTypeAppend(questionType, 1);
  $(".question-type-0").addClass("actived");
  $(".title-text").text(questionType[0].name);
  handleQuestionInfoAppend(questionInfoItems, 10);
  var questionTypePageAppendStr = "";
  var questionTypePageNum = Math.ceil(questionType.length / 10);
  for (i = 0; i < questionTypePageNum; i++) {
    questionTypePageAppendStr +=
      '<div class="question-type-page-content question-type-page-' +
      (i + 1) +
      '">' +
      (i + 1) +
      "</div>";
  }

  $(".question-type-pages").append(questionTypePageAppendStr);
  $(".question-type-page-1").addClass("actived");

  $(".question-type-page-content").click(function () {
    $(".question-type-page-content.actived").removeClass("actived");
    $(this).addClass("actived");
    pageIndex = $(this)[0].className.replace(/[^0-9]/gi, "");
    if (pageIndex) {
      $(".question-type-content").hide();
      $(".question-type-content").fadeIn(
        500,
        handleQuestionTypeAppend(questionType, pageIndex)
      );
    }
  });

  $(".question-info-more-btn svg").click(function () {
    handleQuestionInfoAppend(questionInfoItems, questionInfoItems.length);
    $(".question-info-more-btn svg").hide();
  });
};

function handleQuestionTypeAppend(items, num) {
  var questionTypeAppendStr = "";
  $.each(items, function (index, value) {
    if (index < num * 10 && index >= (num - 1) * 10) {
      questionTypeAppendStr +=
        '<div class="question-type-box-content question-type-' +
        index +
        '"><div class="question-type-box"><div class="question-type-bird"><img class="black-brid" src="./images/bird.svg" alt=""><img class="green-brid" src="./images/bird_green.svg" alt=""></div><div class="question-type-text">' +
        value.name +
        '</div></div><div class="question-type-actived-arrow"><img src="./images/FAQ-sc-2-actived.png" alt=""></div></div>';
    }
  });
  $(".question-type-content").html(questionTypeAppendStr);
  $(".question-type-box-content").click(function () {
    $(".question-type-box-content.actived").removeClass("actived");
    $(this).addClass("actived");
    var questionTypeIndex = parseInt(
      $(this)[0].className.replace(/[^0-9]/gi, "")
    );
    questionInfoItems = items[questionTypeIndex].questionItems;
    $(".question-info-content-box").hide();
    $(".title-text").text(items[questionTypeIndex].name);
    $(".question-info-content-box").fadeIn(
      500,
      handleQuestionInfoAppend(questionInfoItems, 10)
    );
  });
}

function handleQuestionInfoAppend(items, num) {
  var questionInfoItemsAppendStr = "";
  $.each(items, function (index, value) {
    if (index < num) {
      questionInfoItemsAppendStr +=
        '<div class="question-info-content-list question-info-list-' +
        index +
        '"><ol class="question-info-ol"><li>' +
        value +
        '</li></ol><img class="gray-arrow" src="./images/arrow.svg"><img class="green-arrow" src="./images/arrow_green.svg"></div>';
    }
  });
  $(".question-info-content-box").html(questionInfoItemsAppendStr);
  if (items.length > num) {
    $(".question-info-more-btn svg").show();
  } else {
    $(".question-info-more-btn svg").hide();
  }
  $('.question-info-content-list').click(function() {
    window.open('../FaqDetail/FaqDetail.html')
  })
}
