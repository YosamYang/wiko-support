$(function(){
    var width=document.documentElement.clientWidth || window.innerWidth;
    var initIndex=0;
    if (width<768) {
        initIndex=3;
    }else if(width>992){
        initIndex=9;
    }else{
        initIndex=4;
    }

    // 初始化
    function init(){
        $('.products').addClass('hide');
        $('.products').eq(0).addClass('show');
        $('.range .item').eq(0).addClass('active');
    }

    // 只显示初始产品
    function initProducts(index){
        var index=index || 0;
        $('.products .product').addClass('importanthide');
        $('.products').each(function(index,value){
            $(value).find('.product').slice(0,initIndex).removeClass('importanthide');
        })
    }

    // 根据点击，显示不同列表
    $('.range .item').on('click',function(){
        $('.range .item').removeClass('active');
        $('.products').removeClass('show');
        $(this).addClass('active');
        $('.products').eq($(this).index()).addClass('show');
    })
    
    // 显示更多
    $('.more-icon').each(function(index,value){
        this.initIndex=initIndex;
        
        $(value).on('click',function(){
            if (width<768) {
                this.initIndex+=1;
            }else if(width>992){
                this.initIndex+=3;
            }else{
                this.initIndex+=2;
            }
            $('.products:nth-of-type('+(index+2)+') .product').slice(0,this.initIndex).removeClass('importanthide');
        })
    })

    init();
    initProducts();

})