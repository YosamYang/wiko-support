//swiper
$(function () {
  var mySwiper = new Swiper('.swiper', {
    autoplay: {
      delay: 4000,
      disableOnInteraction: false,
    },
    // autoplay: false,
    pagination: {
      el: '.hero-carousel__pagination',
      clickable: true,
    },
  })



//is mob
  function IsPC() {
    var userAgentInfo = navigator.userAgent;
    var Agents = ["Android", "iPhone",
      "SymbianOS", "Windows Phone",
      "iPad", "iPod"
    ];
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
      if (userAgentInfo.indexOf(Agents[v]) > 0) {
        flag = false;
        break;
      }
    }
    return flag;
  }


//text list
var datalist1 = {
  p: "Find the explanations on your phone’s use, accessories and connected devices.",
  li: ["ringtone", "Enable/Disable cellular data (data connection)",
    "Enable/Disable notifications sounds",
    "Enable/Disable the vibration for incoming calls",
    "Enable/Disable the vibration on touch",
    "Modify display font size",
    "Modify stand-by delay",
    "Modify the display brightness level",
    "Modify, enable or remove screen unlock mode",
    "Control data usage"
  ]
}
var datalist2 = {
  p: "asd",
  li: ["2222", "2222", "2222", "22222", "222222"]
}
var datalist3 = {
  p: "asd",
  li: ["33333", "33333", "33333", "33333", "33333"]
}
var datalist4 = {
  p: "asd",
  li: ["4444", "4444", "44444", "4444", "44444"]
}
var datalist5 = {
  p: "asd",
  li: ["5555", "5555", "5555", "5555", "1123"]
}

var datalist = [datalist1, datalist2, datalist3, datalist4, datalist5]


  $(".section3-li").on("click", function () {
    $(".section3 .section3-li-pbox .svg").removeClass("svgtoo")
    $(this).find(".svg").addClass("svgtoo")
    if (IsPC()) {
      $(".section3-li").removeClass("active3")
      $(".section-li-span").removeClass("activebg")
      $(this).addClass("active3")
      $(this).find(".section-li-span").addClass("activebg")
      $(".section3-ul1").text("")
      var content = ""
      content += "<p>" + datalist[$(this).attr("key")].p + "</p>"
      for (let index = 0; index < datalist[$(this).attr("key")].li.length; index++) {
        content += "<li class='section3-li1'><span class='section3-span'></span>" + datalist[$(this).attr("key")].li[index] + "</li>"
      }
      $(".section3-ul1").append(content);
    } else {
      if ($(this).attr("class") == "section3-li") {
        $(".section3-li").removeClass("active");
        $(".section3-li-list").slideUp("slow");
        $(this).addClass("active")
        $(this).find(".section3-li-list").slideDown("slow")
      } else {
        $(".section3-li").removeClass("active");
        $(".section3-li-list").slideUp("slow");
      }

    }

  })

});

//a href
$($(".section2-3 .section2-child")[1]).on("click",function(){
  console.log("asd")
  window.location.href = "//Dlhw5cd9504fsr/天音服务页/服务/page/SoftwareUpgrade/SoftwareUpgrade.html";
})
// $($(".section2-3 .section2-child")[2]).on("click",function(){
//   console.log("asd")
//   window.location.href = "//Dlhw5cd9504fsr/天音服务页/服务/page/SoftwareUpgrade/SoftwareUpgrade.html";
// })
$($(".section2-3 .section2-child")[4]).on("click",function(){
  console.log("asd")
  window.location.href = "//Dlhw5cd9504fsr/天音服务页/服务/page/FAQ/FAQ.html";
})
$($(".section2-3 .section2-child")[5]).on("click",function(){
  console.log("asd")
  window.location.href = "//Dlhw5cd9504fsr/天音服务页/服务/page/Warranty/Warranty.html";
})

$(".section3 .section3-right .section3-li1").on("click",function(){
  console.log("asd")
  window.location.href = "//Dlhw5cd9504fsr/天音服务页/服务/page/FaqDetail/FaqDetail.html";
})
